# toast.py: Command line Gcode Sender #

* Version 0.5 - working reliably on Linux, but I need feedback from other platforms.

toast.py is a command-line utility to send gcode to GRBL devices like laser engravers. It is very simple, and can be scripted as part of a more complex workflow.

## Liability ##

By installing and using toast.py, you accept all responsibility for any damage or harm that may occur, and exclude the author(s) of this software from liability for any damages whatsoever.

## Requirements ##

* Your GRBL device must be running **Grbl 1.1** or later. See [these instructions](https://github.com/gnea/grbl/wiki/Flashing-Grbl-to-an-Arduino).
* You need **Python 3** installed on your system. Type `python --version` on the command line to check, otherwise google how to install it.
* The **pyserial** package. Type `pip3 install pyserial` to install it.

## Installation ##

**Linux:** Copy the `toast .py` and `toast.conf` files into a directory in your path (`/usr/local/bin` is a good candidate, or type `echo $PATH` for a list). Make sure `toast .py` is executable (`chmod u+x toast.py`).

**OSX:** In theory following the Linux instructions should work, but I'd be glad of confirmation or corrections.

**Windows:** If anyone can explain how to install a Python script on Windows, please let me know and I'll update these instructions.

## Operation ##

toast.py needs a gcode file to send to your GRBL device, and the `test.gcode` file which comes with toast.py is a good place to start. It commands the GRBL device to move in a 50mm square (without burning), which is a good check that everything is working.

1. Copy `test.gcode` into a working directory, and change to that directory.
2. Connect your GRBL device to your computer, and turn it on.
3. Type `toast.py test.gcode` and press enter.
4. Watch in amazement as your GRBL device moves in a square:
```
> toast.py test.gcode
Initialising port ttyUSB0...OK
Writing gcode line 6
Sent 6 lines of gcode in 1.52s.
```

If you have more than one active USB port, toast.py will ask you which one you want to use before proceeding. You can override this by setting the port in `toast.conf` (see below).

Now you are ready to send gcode files generated by other applications (perhaps downloaded from  [Toaster](http://toaster.igregious.com/)).

## Advanced ##

### Verbose Mode ###
If you are having problems, you can run toast.py in verbose mode by adding a `-v` argument:
```
    toast.py -v test.gcode
```
In verbose mode, toast.py shows a version number, the settings it loaded, and every line it sends and receives:
```
> toast.py -v test.gcode
toast.py version 0.5
Got settings from /home/gregf/Code/python/toast/toast.conf: Timeout:5 Port:None
Initialising port ttyUSB0...
1.488 Rcvd: '\r\n'
1.489 Rcvd: "Grbl 1.1f ['$' for help]\r\n"
OK
Sent: [G92 X0 Y0]
Rcvd: [ok] (0.003s)
Sent: [G0 X50 Y0]
Rcvd: [ok] (0.004s)
Sent: [G0 X50 Y50]
Rcvd: [ok] (0.004s)
Sent: [G0 X0 Y50]
Rcvd: [ok] (0.004s)
Sent: [G0 X0 Y0]
Rcvd: [ok] (0.004s)
Sent: [M5 G0 X0 Y0]
Rcvd: [ok] (0.004s)
Sent 6 lines of gcode in 1.52s.
```
### Configuring toast.conf ###

You can configure toast.py to your GRBL device by changing settings in `toast.conf` (which must live in the same
directory as `toast.py`).

* **timeout:** Sets how long toast.py waits for an OK response from the GRBL device. This defaults to 5 seconds, but if you have gcode lines that take a long time to execute, you can increase this without any ill effects.

* **port:** Forces toast.py to use the given port. You should not need this unless you are automating toast.py and have more than one active USB ports.

* **start:** toast.py will add the following gcode to the start of every gcode file. So if your GRBL device needs some custom initialisation gcode, this is a good place to add it.

* **end:** toast.py will add the following gcode to the end of every gcode file.

### Return Codes ###
If you're writing a script, toast.py returns the following error codes:

| Code | Description |
|----|----|
| 0  | Success |
| 1  | Missing gcode filename argument |
| 2  | Cannot open gcode filename |
| 3  | Cannot find any active USB ports |
| 4  | Quit from port selection prompt |
| 10 | Cannot open port |
| 11 | Timed out waiting for GRBL |
| 12 | Timed out waiting for OK |
| 13 | Gcode error |
| 14 | Unexpected response |


## Problems? ##

If toast.py doesn't do what you expect, run it again with the -v option to provide verbose messages. This shows exactly what toast.py is sending and recieving, and hopefully will give you a better idea of what is going wrong.

Then try changing things. If it's a comms problem, plug into a different USB port, use a different USB cable, or try sending from a different device (if you have access to one).

If all this fails, you need support - *but you need to ask the right people*. Remember toast.py is just the messenger between the gcode provider and the GRBL device, so if the problem is gcode that GRBL doesn't understand, raising an issue for toast.py won't get your problem fixed.

But if you think the problem really is toast.py, go to [the toast.py issue tracker](https://bitbucket.org/vig/toast.py/issues)
and create an issue. If you know any Python, you can also hack the `toast.py` file to try and
fix the problem - it's a fairly simple script.

