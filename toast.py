#!/usr/bin/env python3
# coding=utf-8

#	toast.py: Commandline gcode sender.
#	Copyright 2020 Greg Fawcett greg@vig.co.nz
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import time
import serial
import serial.tools.list_ports

version='0.5'
conf_filename='toast.conf'


#----------------------------------------------------------
def add_gcode(line, gcode_list):
	if line.startswith('#'):
		return
	try:
		line=line[:line.index('(')]
	except:
		pass
	try:
		line=line[:line.index(';')]
	except:
		pass
	line=line.strip()
	if line:
		gcode_list.append(line)


#----------------------------------------------------------
class Toast(object):

	#------------------------------------------------------
	def __init__(self):
		self.verbose=any(arg.startswith('-v') for arg in sys.argv)
		if self.verbose:
			print('toast.py version %s'%version)
		self.timeout=5
		self.force_port=None
		self.start=time.time()

		# Try to get settings from conf_filename
		start_gcode=[]
		end_gcode=[]
		try:
			rundir=sys.path[0]+os.sep
			with open(rundir+conf_filename, 'r') as settings_file:
				in_start=in_end=False
				for line in settings_file:
					uline=line.upper()
					if uline.startswith('TIMEOUT:'):
						in_start=in_end=False
						try:
							self.timeout=int(line[8:].strip())
						except ValueError:
							print('Cannot parse timeout in %s. Falling back to default (%s secs).'%(conf_filename, self.timeout))
					if uline.startswith('PORT:'):
						self.force_port=line[5:].strip()
					elif uline.startswith('START:'):
						in_start=True
						in_end=False
					elif uline.startswith('END:'):
						in_start=False
						in_end=True
					else:
						if in_start:
							add_gcode(line, start_gcode)
						elif in_end:
							add_gcode(line, end_gcode)
			self.debug('Got settings from %s (Timeout:%d Port:%s)'%(rundir+conf_filename, self.timeout, self.force_port if self.force_port else 'None'))
		except FileNotFoundError:
			print('Cannot open %s. Falling back to default values.'%(rundir+conf_filename))
			start_gcode.append('G92 X0 Y0')
			end_gcode.append('M5 G0 X0 Y0')

		# Load gcode from file.
		self.gcode=list(start_gcode)
		gcode_loaded=False
		for arg in sys.argv[1:]:
			if arg.startswith('-v'):
				continue
			elif gcode_loaded==False:
				try:
					with open(arg, 'r') as gcode_file:
						for line in gcode_file:
							add_gcode(line, self.gcode)
						gcode_loaded=True
				except FileNotFoundError:
					self.die('Cannot open gcode file %s.'%arg, 2)
			else:
				print('Ignoring command line argument (%s)'%arg)
		if not gcode_loaded:
			self.die('Gcode file name is missing.', 1)
		self.gcode.extend(end_gcode)

	#------------------------------------------------------
	def debug(self, message):
		if self.verbose:
			print(message)

	#------------------------------------------------------
	def select_port(self):
		ports=[p for p in serial.tools.list_ports.comports() if p.vid is not None]
		if self.force_port: # Try to find port specified in toast.conf
			for port in ports:
				if port.name==self.force_port:
					return port
		if len(ports)==0:
			self.die('Cannot find any active USB ports.', 3)
		elif len(ports)==1:
			return ports[0]
		else:
			print('Select a USB port:')
			for i, port in enumerate(ports):
				print('    %d: %s'%(i+1, port.name))
			while True:
				response=input('Enter a number (or "q" to quit):').strip()
				if response[0]=='q' or response[0]=='Q':
					sys.exit(4)
				try:
					pi=int(response)-1
				except ValueError:
					continue
				if 0<=pi<len(ports):
					return ports[pi]


	#------------------------------------------------------
	def open_port(self, port):
		if self.verbose:
			print('Initialising port %s...'%port.name)
		else:
			sys.stdout.write('Initialising port %s...'%port.name)
			sys.stdout.flush()
		try:
			self.port=serial.Serial(port.device, baudrate=115200, timeout=self.timeout)
		except serial.SerialException as err:
			self.die('cannot open port (%s).'%err, 10)
		# Wait up to 5 seconds for GRBL boot message
		start=time.time()
		while True:
			response=self.port.read_until(b'\n').decode('ascii')
			duration=time.time()-start
			self.debug('%0.3f Rcvd: %s'%(duration, repr(response)))
			if response.upper().startswith('GRBL'):
				print('OK')
				return
			if duration>5:
				self.die('timed out waiting for GRBL.', 11)

	#------------------------------------------------------
	def close_port(self):
		self.port.close()

	#------------------------------------------------------
	def send_line(self, line):
		self.port.write(line.encode('ascii')+b'\r')
		self.debug('Sent: [%s]'%line)
		start=time.time()
		response=self.port.read_until(b'\n').decode('ascii')
		duration=time.time()-start
		self.debug('Rcvd: [%s] (%0.3fs)'%(response.strip(), duration))
		if not response.endswith('\n'):
			self.die('Timed out waiting for OK after sending [%s]'%line, 12, True)
		response=response.upper().strip()
		if response.startswith('ERROR'):
			self.die('Gcode error (%s) after sending [%s].'%(response, line), 13, True)
		if not response.startswith('OK'):
			self.die('Unexpected response (%s) after sending [%s].'%(response, line), 14, True)
		return

	#------------------------------------------------------
	def send_gcode(self):
		for i, line in enumerate(self.gcode):
			if not self.verbose:
				sys.stdout.write('\033[G\033[KWriting gcode line %d'%(i+1))
				sys.stdout.flush()
			self.send_line(line)
		if not self.verbose:
			print()
		print('Sent %d lines of gcode in %0.2fs.'%(len(self.gcode), time.time()-self.start))


	#------------------------------------------------------
	def die(self, message, return_code, inline=False):
		if self.verbose or not inline:
			print(message)
		else:
			print('\n'+message)
		sys.exit(return_code)


#----------------------------------------------------------
# Main
if __name__ == '__main__':
	# Load settings, if available.
	toast=Toast()
	# Establish serial port
	toast.open_port(toast.select_port())
	# Send gcode
	toast.send_gcode()
	toast.close_port()
